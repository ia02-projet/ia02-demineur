"""
@file map_solver.py
Contient les classes et fonction nécessaire à la résolution d'une map
"""

from typing import List, Tuple, Dict
from pprint import pprint
import copy
from random import choice

from solver_utilities import SolverUtil, cell_to_variable, substractDiscoveredAnimals
from crocomine_client import CrocomineClient
from gophersat_management import Gophersat
from project_typing import Variable, GrilleDict, Infos, Status
from enumAnimal import Animal, strAnimalToEnum, enumAnimalToStr
from printer import GridPrinter
from debug_utilities import Debug

#Global variables
sea_count = None
land_count = None

# Classe pour la résolution des maps, elle contient une
# fonction statique permettant de jouer et de résoudre une map,
# cette fonction retourne des information sur la résolution de la map
class MapSolver:
	# boucle de résolution de la grille
	# retourne le nombre d'actions réalisées sur le serveur et si la map a été win ou non
	@staticmethod
	def runMap(grid: GrilleDict, client: CrocomineClient, gridInfos : Dict) -> Tuple[int,bool]:
		debug = Debug.instance()

		# Récupération du nombre d'animaux global de la grille
		animalCount = gridInfoParsing(gridInfos)
		serverCalls = 0
		while(1):
			debug.debugPrint("==========================================")
			debug.debugPrint("============== Nouveau Tour ==============")
			debug.debugPrint("==========================================")

			# Requête de résolution à gophersat, création d'une grille de guess
			# permettant de faire des requêtes au server
			gridGuess = doRequestsOnCells(grid, animalCount)

			#on guess d'abord les animaux pour faire des choord après
			status, calls, msg = guessAnimals(grid, gridGuess, client)
			serverCalls += calls
			# Gestion du status de retour et test de la fin de la map
			try:
				finished, win = isMapFinished(status, msg)
				if finished:
					return (serverCalls, win)
			except Exception as e:
				debug.infoPrint(str(e))
				debug.infoPrint("Try to continu the round")

			# Découverte de toutes les cases dont les voisins sont guess ou safe
			# optimisation des découvertes
			status, calls, msg = doChords(grid, gridGuess, client)
			serverCalls += calls
			# Gestion du status de retour et test de la fin de la map
			try:
				finished, win = isMapFinished(status, msg)
				if finished:
					return (serverCalls, win)
			except Exception as e:
				debug.infoPrint(str(e))
				debug.infoPrint("Try to continu the round")

			# On fait les discover restant qui ne peuvent se faire par chord
			status, calls, msg = discoverSafeCells(grid, gridGuess, client)
			serverCalls += calls
			# Gestion du status de retour et test de la fin de la map
			try:
				finished, win = isMapFinished(status, msg)
				if finished:
					return (serverCalls, win)
			except Exception as e:
				debug.infoPrint(str(e))
				debug.infoPrint("Try to continu the round")

# Récupération des infos serveur concernant le nombres d'animaux
def gridInfoParsing(gridInfos: Dict) -> List[int]:
	animalCount = [None, None, None]
	if "tiger_count" in gridInfos:
		animalCount[0] = gridInfos["tiger_count"]
	if "shark_count" in gridInfos:
		animalCount[1] = gridInfos["shark_count"]
	if "croco_count" in gridInfos:
		animalCount[2] = gridInfos["croco_count"]
	if "sea_count" in gridInfos:
		sea_count = gridInfos["sea_count"]
	if "land_count" in gridInfos:
		land_count = gridInfos["land_count"]
	return animalCount

# Test de la fin de la map en fonction du status, gestion du status Err
def isMapFinished(status : Status, msg) -> Tuple[bool,bool]:
	if status == "GG":
		Debug.instance().infoPrint("Map finished : "+status)
		return (True, True)
	elif status == "KO":
		Debug.instance().infoPrint("Map finished : "+status)
		return (True, False)
	elif status == "Err":
		raise Exception(status + " : Server Error : " + msg)
	else:
		return (False, False)

# Traite les infos retrounées par le serveur.
# Met à jour la grille locale avec ces informations
def updateGrid(grid: GrilleDict, gridGuess: GrilleDict, infos: Infos, msg: str):
	debug = Debug.instance()
	debug.debugPrint("Réponse du serveur : " + msg)
	debug.debugPrint("Informations collectées : ")
	debug.debugPprint(infos)
	# Mise à jour de la grille locale
	SolverUtil.updateGrid(grid, infos)
	GridPrinter.printGridAndGridGuess(grid, gridGuess)


# Envoie un guess au serveur pour tous les animaux découverts dans gridGuess
# Retourne les informations sur l'état de la découverte et le nombre d'appels serveurs
def guessAnimals(grid: GrilleDict, gridGuess: GrilleDict, client: CrocomineClient) -> Tuple[Tuple[Status, int],str]:
	M=len(grid)
	N=len(grid[0])
	serverCalls = 0
	for i in range(M):
		for j in range(N):
			# si la case n'est pas découverte (type '?' sur la grille)
			# on check si gophersat a trouvé un animal
			if grid[i][j]["type"] == Animal.UNKNOWN:
				typeCase = gridGuess[i][j]["type"]
				if typeCase == Animal.SHARK or typeCase == Animal.TIGER or typeCase == Animal.CROCODILE:
					Debug.instance().debugPrint("Prochaine action : guess(" + str(i) + ", " + str(j) + ")")
					if askContinue():
						status, msg, infos = client.guess(i,j,enumAnimalToStr(typeCase))
						serverCalls += 1
						if status == "OK":
							updateGrid(grid, gridGuess, infos, msg)
						else:
							return (status, serverCalls, msg)
	return ("OK", serverCalls, "")


# Envoie un discover au serveur pour toutes les cases sans dangers dans gridGuess
# Retourne les informations sur l'état de la découverte et le nombre d'appels serveurs
def discoverSafeCells(grid: GrilleDict, gridGuess: GrilleDict, client) -> Tuple[Tuple[Status, int],str]:
	M=len(grid)
	N=len(grid[0])
	serverCalls = 0
	for i in range(M):
		for j in range(N):
			# Si la case est inconnue et que Gophersat l'a annoncé comme safe
			if (grid[i][j]["type"] == Animal.UNKNOWN) and (gridGuess[i][j]["type"] == Animal.NO_ANIMAL):
				Debug.instance().debugPrint("Prochaine action : discover sur (" + str(i) + ", " + str(j) + ")")
				if askContinue():
					status, msg, infos = client.discover(i,j)
					serverCalls += 1
					if status == "OK":
						updateGrid(grid, gridGuess, infos, msg)
					else:
						return (status, serverCalls, msg)
	return ("OK", serverCalls, "")

# compte les animaux découverts dans une liste de cellules.
# cette fonction est utilisée dans doChords
def countAnimals(grid: GrilleDict, listCell: List[Tuple[int, int]]) -> List[int]:
	prox_count = [0, 0, 0]
	for coord in listCell:
		#si la cellule est un animal
		cell = grid[coord[0]][coord[1]]
		if cell["type"] < 3:
			prox_count[cell["type"]] += 1
	return prox_count

# Envoie un chord au serveur sur les cases dont il reste des voisins non découverts
# et dont les animaux ont été entièrement découverts
# Retourne les informations sur l'état de la découverte et le nombre d'appels serveurs
def doChords(grid: GrilleDict, gridGuess: GrilleDict, client: CrocomineClient) -> Tuple[Tuple[Status, int],str]:
	M=len(grid)
	N=len(grid[0])
	continu = True
	serverCalls = 0

	# Tant que de nouveaux chord sont possibles
	while(continu):
		maxUndiscoveredProx = 0
		(imax, jmax) = (0,0)
		for i in range(M):
			for j in range(N):
				cell = grid[i][j]
				#on parcours les cellules découvertes sans danger
				if cell["type"] == Animal.NO_ANIMAL:
					undiscoveredProx, discoveredProx = SolverUtil.getProx(grid, i, j)
					discoveredAnimalCount = countAnimals(grid, discoveredProx)
					# si tous les animaux ont été découverts dans le voisinage de la cellule
					if discoveredAnimalCount == cell["prox_count"]:
						#on trouve la case qui a le plus de voisins non découverts
						if len(undiscoveredProx) > maxUndiscoveredProx:
							maxUndiscoveredProx = len(undiscoveredProx)
							(imax, jmax) = (i,j)
		# si il n'y a plus de cases à découvrir
		# ou que le chord permet de découvrir une seule case, on arrête
		if maxUndiscoveredProx <= 1:
			continu = False
			return ("OK", serverCalls, "")
		else:
			Debug.instance().debugPrint("Prochaine action : choord(" + str(imax) + ", " + str(jmax) + ")")
			if askContinue():
				#on fait un choord sur la case max
				status, msg, infos = client.chord(imax, jmax)
				serverCalls += 1
				if status == "OK":
					updateGrid(grid, gridGuess, infos, msg)
				else:
					return (status, serverCalls, msg)
	return ("OK", serverCalls, "")

# retourne une gridGuess en effectuant des requêtes gophersat sur le type des cases
def doRequestsOnCells(grid: GrilleDict, animalCount : List[int]) -> GrilleDict:
	M=len(grid)
	N=len(grid[0])
	debug = Debug.instance()
	filename = "demineur.cnf"
	gophersat = Gophersat(filename)
	gridGuess = copy.deepcopy(grid)
	# On transforme la grille en information DIMACS
	cnf = SolverUtil.gridToDimacs(grid)

	unknownCell = []
	numUnkownPerihpCell = 0
	numIgnoreCell = 0
	periphCell = []
	block = True
	needRandom = False


	# Si il y a blocage on tente :
	# 	- L'ajout de l'information du compte d'animaux global resant
	#	- Si toujours bloqué on choisit une case de façon aléatoire
	while(block):
		for i in range(M):
			for j in range(N):
				# on récupère les cellules à deviner
				if grid[i][j]["type"] == Animal.UNKNOWN:
					debug.debugPrint("Prochaine action : requête sur (" + str(i) + ", " + str(j) + ")")
					if askContinue():
						#on itère (à l'envers) sur TIGER, SHARK, CROCODILE et NO_ANIMAL
						for type in range(Animal.NO_ANIMAL, Animal.TIGER-1, -1):
							clause = [-cell_to_variable(i,j,type)]
							cnf.addClause(clause)
							cnf.dimacsToFile(filename)
							if gophersat.isUnsat():
								block = False
								needRandom = False
								# On soustrait l'animal trouvé au compte global
								subAnimal(animalCount,type)
								gridGuess[i][j]["type"] = type
								debug.debugPrint("Found (" + str(i) + ", " + str(j) + ") = " + enumAnimalToStr(type))
								debug.debugPrint("New gridGuess : ")
								GridPrinter.printGridAndGridGuess(grid, gridGuess)
								cnf.removeClause(clause)
								# On itère pas inutilement sur les autres type d'animaux
								break
							debug.debugPrint("Cell " + enumAnimalToStr(type) + " : UNSAT")
							cnf.removeClause(clause)
		
		# Premier blocage, ajout d'information
		if(block and not needRandom):
			debug.debugPrint("Blocked. Adding animal count information")

			# Si toujours bloqué à la prochaine itération on sélectionnera une case random
			needRandom = True
			# Ajout d'informations utiles sur les cases réstantes
			for i in range(M):
				for j in range(N):
					if "prox_count" in grid[i][j]:
						periphCell.append(((i,j), grid[i][j]))
					if (grid[i][j]["type"] == Animal.UNKNOWN) or (grid[i][j]["type"] == Animal.IGNORE):
						unknownCell.append((i,j))
					if (grid[i][j]["type"] == Animal.UNKNOWN):
						numUnkownPerihpCell += 1
					if (grid[i][j]["type"] == Animal.IGNORE):
						numIgnoreCell += 1
			# On tente l'ajout de l'information du nomvre d'animaux total, si celui-ci échou on prend une case aléatoire
			if SolverUtil.useAnimalCountDimacs(animalCount,cnf,unknownCell, numUnkownPerihpCell, numIgnoreCell, grid):
				selectRandomCell(grid, gridGuess, unknownCell, periphCell, animalCount)
				block = False
		# Si il y a eu blocage malgré l'information supplémentaire on choisi une case aléatoire
		elif needRandom:
			selectRandomCell(grid, gridGuess, unknownCell, periphCell, animalCount)
			block = False

	return gridGuess

def selectRandomCell(grid : GrilleDict,
					 gridGuess : GrilleDict,
					 unknownCell: List[Tuple[int,int]],
					 periphericCell: List[Tuple[int,int]],
					 animalCount : List[int]):

	debug = Debug.instance()
	debug.debugPrint("RANDOM SELECTION")

	# Première méthode on choisit une case parmis les cases restantes non découverte
	# On en récupère la probabilité de tomber sur un animal
	(i_uk,j_uk), chanceToHitAnimalUnknownCell = getRandomCellFromUnknownCell(unknownCell,animalCount)

	# Seconde method : on regarde les cell périphériques et on prend celle qui a le moins de
	# voisin dangereux
	(i_p,j_p), chanceToHitAnimalPeriphCell = getRandomCellFromPeriphCell(periphericCell, grid)

	# On compare les probabilités des 2 méthodes et on prend la plus petite
	if chanceToHitAnimalPeriphCell < chanceToHitAnimalUnknownCell :
		(i,j) = (i_p,j_p)
		debug.debugPrint("Rand from peripheric cell")
	else:
		(i,j) = (i_uk,j_uk)
		debug.debugPrint("Rand from unknown cell")

	# On indique que la case découverte est safe dans les 2 grilles
	gridGuess[i][j]["type"] = Animal.NO_ANIMAL
	grid[i][j]["type"] = Animal.UNKNOWN

	debug.debugPrint("RAND : " + str(i) +";"+str(j))


# On choisit une case parmis les cases restantes non découverte
def getRandomCellFromUnknownCell(unknownCell: List[Tuple[int,int]], animalCount: List[int]) -> Tuple[Tuple[int,int],float]:
	animalLeft = 0
	c = 0
	for i in range(3):
		if animalCount[i] is not None:
			c+=1
			animalLeft += animalCount[i]
	randOnUnknownCellProb = animalLeft/len(unknownCell)

	# Si on a pas assez d'information sur le nombre d'animaux
	# ne devrait pas arriver
	(i,j) = choice(unknownCell)
	if c < 3:
		return ((i,j), 1)
	else:
		return ((i,j),randOnUnknownCellProb)


# Second method : On parcourt la liste des cellules
# On regarde les cellules à proximité
# On choisit un des voisins à proximité de la cellule qui a le ratio
# nombreAnimauxProches / casesNonDécouvertesProches le plus petit
# (proba de tomber sur un animal la plus petite)
def getRandomCellFromPeriphCell(periphericCell: List[Tuple[int,int]], grid : GrilleDict ) -> Tuple[Tuple[int,int],float]:
	minChanceToHitAnimal = 1
	minUndiscoveredProx = []
	for cells in periphericCell:
		(i,j) = cells[0]
		cell = cells[1]
		undiscoveredProx, discoveredProx = SolverUtil.getProx(grid, i, j)
		if undiscoveredProx != []:
			undiscoveredProxCount = substractDiscoveredAnimals(cell, grid, undiscoveredProx, discoveredProx)
			animalProx = sum(undiscoveredProxCount)
			chanceToHitAnimal = animalProx / len(undiscoveredProx)
			if chanceToHitAnimal < minChanceToHitAnimal:
				minChanceToHitAnimal = chanceToHitAnimal
				# Sauvegarde des cases non découverte de plus grand nombre
				minUndiscoveredProx = undiscoveredProx

	# Si des solutions ont été trouvé on choisit une case aléatoire parmis
	# les cases non découverte de la cellule de proximité
	# Sinon renvoie une probabilité de 1
	if minUndiscoveredProx != []:
		(i,j) = choice(minUndiscoveredProx)
		return ((i,j),minChanceToHitAnimal)
	else:
		return ((0,0),1)

# On soustrait un animal au total de ce type d'animal
def subAnimal(animalList : List[int], types: Animal):
	if types < 3:
		if animalList[types] != None:
			animalList[types] -= 1

# Debug pour le pas à pas
def askContinue():
	if Debug.instance().debug:
		continu = input('Continuer ? - o / n - : ') == 'o'
		if continu:
			return True
		else:
			quit()
	else:
		return True
