"""
@file debug_utilities.py
Class Debug de redirection des print
Définition de différents niveaux de debug. Les appels à print() sont vérifiés
en fonction du niveau de debug souhaité
"""

from pprint import pprint

# Class used to handle the debuging process with specific printing
# and boolean var to check the level of debug
class Debug(object):
	_instance = None

	# Debug properties set in the argument parsing solver.py
	debug = False
	debugP = False
	server_map = False

	def __init__(self):
		raise RuntimeError('Call instance() instead')

	# Singleton patern to get an unique instance of the class
	@classmethod
	def instance(cls):
		if cls._instance is None:
			cls._instance = cls.__new__(cls)
		return cls._instance

	# Different printing function
	def debugPrint(self, string: str):
		if self.debugP:
			print(string)

	def infoPrint(self, string : str):
		print(string)

	def debugPprint(self, string: str):
		if self.debugP:
			pprint(string)

	def infoPprint(self, string : str):
		pprint(string)
