# IA02 Démineur d'arité 3

Projet de l'UV IA02 pour le semestre P21.

## Sujet

On considère un monde 2D sous forme de grille de taille M*N (M lignes et N colonnes). Les bords ne sont pas connectés.

- Une case est soit de type **terrestre**, soit de type **aquatique**.
- On rencontre parfois des animaux dangereux : des **tigres**, des **requins**, et des **crocodiles** sur les cases
- Les tigres vivent sur la terre ferme, les requins vivent dans l’eau, et les crocodiles sont capables de vivre sur ces 2 habitats.
- Sur une même case, il ne peut y avoir qu’un seul animal.
- Au départ est donné M, N, le nombre de tigres, le nombre de requins et le nombre de crocodiles, le nombre de terre et le nombre de mer et d’éventuelles statistiques sur la difficulté de la carte (ex. l’indice 3BV).
- Pour limiter le hasard, une case de départ, sans danger, est également donnée.
- Les cases sans animaux possèdent des informations sur les animaux à proximité, sous la forme d'un **triplet de 3 entiers** _[int, int, int]_. Ce triplet contient respectivement, le nombre de tigres, le nombre de requins et le nombre de crocodiles dans le voisinage de la case.
- 3 actions sont possibles à chaque tour: découvrir une tuile (**discover**), deviner un tigre ou un requin ou un crocodile (**guess**) et enfin découvrir en une fois l’ensemble des cases non découvertes autour d’une case dont tous les animaux ont déjà été trouvés (**chord**).
Toute erreur entraîne la mort de l’explorateur et la fin de la carte.

On demande de réaliser un joueur artificiel capable de communiquer avec un serveur de jeu arbitre, de lui communiquer ses coups et de prendre en compte les informations fournies en retour, de manière à deviner l’emplacement de tous les animaux et de découvrir toutes les autres cases.

La solution choisie est l'utilisation d'un solveur SAT piloté à l’aide d’un programme écrit en Python.

## Utilisation

Nécessite _python3_ installé.
Le programme _main_ est _solver.py_.

> $ python3 solver.py

Cette commande lance un serveur arbitre _crocomine\_lite_ directement dans le code puis résoud les maps chargées par le serveur du dossier _grids_.

Des options peuvent être données en argument.

``` bash
$ usage: python3 solver.py [options] ...  
	Options and arguments:  
	-d, --debug  
		Launch in debug mode, printing maps and pausing at each action  
	-p, --print  
		Print debug informations and maps, without pausing  
	-m, --server-map  
		Print beautify server map evolution.  
	-s [server_adress], --server [server_adress]  
		Use an external server  
	-c, --csv  
		Stat export in csv in file stats.csv  
	-h, --help  
		Display this help  
```

Notamment l'option -s qui permet de se connecter à un serveur _crocomine\_lite_ externe.
``` bash
$ ./crocomine_lite :8000 [grid_directory_path] # lancement du serveur externe  
$ python3 solver.py -s http://localhost:8000 # connexion au serveur à l'adresse :8000
```

Un générateur de map est également fourni. Les paramètres de taux d'animal et de taille de la grille peuvent être directement modifiés à l'intérieur du script.

```bash
$ python3 map_generator 100 --verbose #génère 100 maps
```


## Modélisation du problème en logique booléenne

Pour pouvoir résoudre la grille, il faut modéliser la base de connaissance acquise en logique booléenne. Tout cela est effectué dans le fonction _gridToDimac(grid)_ dans le fichier _solver_utilities.py_. Elle prend en entrer la grille de jeu courante et fournit en sortie un objet _Dimac_ contenant la liste des clauses associées à la grille.

### Variables booléennes

Le problème est modélisé à l'aide de 4 catégories de variables booléennes. En effet, une case peut avoir plusieurs types :
- Soit elle contient un **tigre**
- Soit elle contient un **requin**
- Soit elle contient un **crocodile**
- Soit elle est **sans danger**

Ce qui représente au total 4 possibilités.

On modélise donc le problème à l'aide de M * N * 4 variables booléenne :
- _Variable 1 :_ **S(0,0)T** -> La case (0,0) est un tigre
- _Variable 2 :_ **S(0,0)S** -> La case (0,0) est un requin
- ...
- _Variable M\*N\*4 :_ **S(M\*N, M\*N)SAFE** -> La case (M\*N, M\*N) est sans danger

Une variable représente une possibilité pour une case. On a donc 4 variables par case.

### Contraintes clausales

On modélise les règles du jeu en logique booléenne. On utilise pour cela les contraintes *at_most_n* et *at_least_n*, qui, réunie permettent de définir une contrainte *exactly_n*. Cette contrainte, disponible dans le fichier _clausal_constraints.py_, prend en paramètre une liste de variable, et renvoie la liste de clause associée à la phrase _"Exactement n variables parmis la liste sont vraies"_

### Chaque case ne peut avoir qu'un seul type

Il s'agit d'un XOR. On veut que parmi les 4 variables pour une case, seulement une seule soit vraie (une case ne peut pas contenir à la fois un tigre et un requin par exemple). Il s'agit d'une contrainte _exactly_n_ avec _n = 1_.

### Cases déjà découvertes

Les cases déjà découvertes sont modélisées en définissant la variable correspondant au type de la case comme _Vraie_.

### Informations de proximité

Chaque case sans danger possède une information de proximité sur ses voisins dangereux.
On connaît également le type de terrain des cases à proximité.
On modélise cette information comme ceci :
- Itération sur les cases découvertes qui possèdent une information de proximité
	- Récupération de la liste des voisins non découverts
	- Récupération des variables _TIGRE_ de ces cases
	- Pour un nombre de tigres égal à T :
		- on applique la contrainte _exactly_n_ sur les cases voisines qui ont le type _"land"_
		- on applique la contrainte _exactly_0_ sur les cases voisines qui ont le type _"sea"_ (il ne peut y avoir de tigre sur les cases eau)
	- On fait pareil pour les variables _REQUIN_ et _CROCODILE_ (en adaptant les contraintes aux types de cases)

On obtient un ensemble de clauses modélisant les informations de proximité des cases.

### Nombre total d'animaux

Au début de la carte, une information sur le nombre total de tigres, de requins et de crocodiles est disponible. On peut donc traquer le nombre d'animaux restants lors du déroulement de la partie. Il s'agit ici également d'une contrainte _exactly_n_, mais sur toutes les cases non découvertes de la carte.

Cela génère un nombre de clause très grand si on ajoute cette contrainte dès le début, car le nombre de combinaisons est une fonction factorielle. Il faut donc utiliser cet information intelligemment, uniquement lorsque celà peut se révéler utile (plus de détail sur notre stratégie dans la section _Stratégie de résolution_).

## Modélisation d'une case sous forme de dictionnaire (objet)

Une case est représentée par un dictionnaire python de cette forme :
``` python
# case {
#	"field" : "sea" or "land"
#	"prox_count" : [int, int ,int] -> nombre de tigres, requins, crocodiles à proximité
#	"type" : int -> 0 pour tigre, 1 pour requin, 2 pour crocodile, 3 pour aucun animal, 4 pour
#					cellule ignorée, 5 pour cellule non découverte
# }
```

On peut accéder à une case en connaissant ses coordonéees (i,j) dans la grille.
La case peut avoir 4 types définis dans _enumAnimal.py_ :

``` python
class Animal(IntEnum):
TIGER = 0
SHARK = 1
CROCODILE = 2
NO_ANIMAL = 3
IGNORE = 4
UNKNOWN = 5
```

Les cases marquées **IGNORE** sont les cases dont on ne peut rien déduire car aucune information n'a été reçue de la part du serveur. Elles sont donc ignorées dans la phase de résolution (voir section _Stratégie de résolution_) et sont marquées d'un symbôle '|' lors de l'affichage de la grille. Les cases marquées **UNKNWON** sont les cases qui ne sont pas encore découvertes mais sur lesquelles on possède une information : elles sont donc potentiellement trouvables. Elles sont marquées d'un symbôle '?' lors de l'affichage de la grille.

Alias de type pour modéliser la grille qui stocke les cases :

``` python
GrilleDict = List[List[Dict]]
```

2 types de grilles sont utilisées :
- la grille principale dite _grid_ qui n'est mise à jour que grâce aux infos du serveur
- la grille dite _gridGuess_, qui est utilisée pour stocker les résultats du solver SAT avant de les soumettre au serveur.

Pour afficher la grille lors de l'exécution, utilisez l'option _-p_ :

> $ python3 solver.py -p

## Stratégie de résolution

Afin de jouer le moins possible de coups, la stratégie suivante a été adoptée (visible dans la méthode _runMap()_ du fichier _map\_solver.py_):

1. Modélisation de la grille en _dimacs_
2. Iteration sur les cases de types _UNKNOWN_ de la grille (les cases potentiellement devinables). Pour chacune de ces cases, des appels au solver SAT sont effectués pour déterminer le type de la case :
	- On ajoute la négation de la variable correspondant au type à trouver (exemple : not(1) correspond à "la case (0,0) n'est pas un tigre")
	- Si le solver SAT ne trouve pas de modèle c'est donc que la case est forcément du type ciblé (ici un tigre)
	- On effectue ces requêtes au solver SAT jusqu'à trouver le bon type.  
Il s'agit de la méthode _doRequestOnCells()_
3. Une fois le type des cases _UNKNOWN_ trouvé grâce à l'étape précédente les requêtes sont envoyés au serveur dans cette ordre :
	- Découverte des animaux trouvés (envois de _guess_ au serveurs) : fonction _guessAnimals()_
	- Une fois tous les animaux trouvés, on peut effectuer des _chord_ sur les cases dont les animaux a proximité ont été tous découverts. Cela permet d'optimiser le nombre de coup joués en factorisant les _discover_ en un seul appel : fonction _doChords()_
	- Après avoir effectué des _chord_, on effectue les derniers _discover_ sur les cases restantes : fonction _discoverSafeCells()_

Ceci représente la stratégie dans le meilleur des cas, c'est à dire le cas ou le solveur SAT réussit à deviner le type **d'au moins une case**.
Cependant, il est très fréquent que le solveur n'ait pas assez d'informations pour déduire quelque chose. Il est alors bloqué.

Dans ce cas il est parfois (je dis bien parfois !) possible de le débloquer en ajoutant l'information **globale** du nombre restant de tigres, requins et crocodiles non découverts sur la carte. Cette contrainte _exactly\_n_ sur l'ensemble des cases restantes peut cependant générer un nombre de clauses très important, ce qui ralentit fortement le solveur SAT (il s'agit d'un problème exponentiel de la classe NP). Une stratégie de _seuil_ a donc été adoptée pour rajouter cette contrainte uniquement dans les cas pertinents. La contrainte est ajoutée dans la modélisation si :
1. **Si le nombre de cellule _IGNORE_ est inférieur au nombre de cellule _UNKNOWN_.** En effet, cette condition permet d'ajouter la contrainte seulement en fin de partie, lorsqu'il y a une possibilité que les informations _globales_ se croisent avec les informations _de proximité_. Autrement, l'information globale est (a priori) inutile.
2. **Si le nombre de combinaisons générées est inférieur à un seuil fixé (ici 1 000 000).** Ce nombre est calculé grâce à la formule du coefficient binomial _k_ parmi _n_. Cela permet d'éviter de générer trop de clause et de ralentir le programme.
Cette stratégie permet d'avoir un compromis entre temps de calcul et débloquage du solveur. Elle est programmée dans la fonction _useAnimalCountDimacs()_ de _solver_utilities.py_

Et enfin, si l'ajout des contraintes globales ne passe pas les critères ci-dessus, ou ne permet toujours pas de débloquer le solveur un choix au hasard doit être réalisé (fonction _selectRandomCell()_ de _map_solver.py_). Ici, 2 stratégies sont effectuées avec calcul de probabilité pour choisir la meilleur :
1. Choix totalement aléatoire sur une case restante de la map (cases marquées _IGNORE_ ou _UNKNOWN_). La probabilité de toucher une case dangereuse et de perdre est donc de **NombreCaseRestantes / NombresAnimauxRestants**.
2. On parcourt la liste des cases des cases ayant une information de proximité :
``` python
for i in range(M):
	for j in range(N):
		if "prox_count" in cells:
			...
```
Pour chacune de ces cases on récupère son nombre de voisins non découverts ainsi que le nombre d'animaux non découverts à proximité. On calcule la probabilité de toucher un animal parmi les voisins non découverts : P = nbAnimauxNonDécouverts / nbVoisinsNonDecouverte.
On sélectionne finalement la case ayant la plus faible probabilité de perdre. Une case voisine au hasard est choisie pour être _discover_.

La méthode ayant la probabilité la plus faible de perdre parmis ces 2 techniques est sélectionnée pour le choix aléatoire.
