# ======================================================================================
# ======================== Générateur de grille aléatoire ==============================
# ======================================================================================

from typing import Tuple, Dict, List
import random
import numpy as np
#for command line agruments
import sys

from enumAnimal import Animal

# Prend la grille générée en entrée et génère le fichier .croco associé
def exportMap(gridName: str, grid: List[List[Dict]], startPoint: Tuple[int, int], M: int, N: int):
	mapStr = "Gen-Map : " + gridName + "\n"
	mapStr += str(M) + " " + str(N) + "\n"
	mapStr += str(startPoint[0]) + " " + str(startPoint[1]) + "\n"
	for i in range(len(grid)):
		for j in range(len(grid[i])):
			cell = grid[i][j]
			if cell["type"] == Animal.TIGER:
				mapStr += "T "
			elif cell["type"] == Animal.SHARK:
				mapStr += "S "
			elif cell["type"] == Animal.CROCODILE:
				if cell["field"] == "sea":
					mapStr += "W "
				else:
					mapStr += "C "
			#safe cell
			else:
				if cell["field"] == "sea":
					mapStr += "~ "
				else:
					mapStr += "- "
		mapStr += "\n"
	with open("./grids/gen-" + gridName + ".croco", "w", newline="") as gridFile:
		gridFile.write(mapStr)

# génère une grille aléatoire en fonction des paramètres donnés
# M,N : taille de la carte
# animalCount : respectivement nombre de tigres, requins et crocodiles
# landProb : entre 0 et 1. Influence la probabilité d'occurence d'une case terre
def generateMap(gridName: str, M: int, N: int, animalCount: Tuple[int, int, int], landProb: float):
	if landProb < 0 or landProb > 1:
		raise Exception("Veuillez fournir une probabilité entre 0 et 1 pour le dernier agrument")
	if sum(animalCount) > M*N:
		raise Exception("Trop d'animaux demandés pour la taille de la map")

	grid = [ [{"type": Animal.NO_ANIMAL} for i in range(N)] for j in range(M)]

	#on initialise la carte avec des cases safe
	for i in range(M):
		for j in range(N):
			prob = random.random()
			grid[i][j]["field"] = "land" if prob < landProb else "sea"


	possibleAnimals = []
	for animal in [Animal.TIGER, Animal.SHARK, Animal.CROCODILE]:
		if animalCount[animal] > 0:
			possibleAnimals.append(animal)

	safeCells = [(i,j) for i in range(M) for j in range(N)]

	#on tire au sort des emplacements pour placer les animaux
	while animalCount != [0,0,0]:
		animal = random.choice(possibleAnimals)
		(x,y) = random.choice(safeCells)
		safeCells.remove((x,y))
		grid[x][y]["type"] = animal
		if animal == Animal.TIGER:
			grid[i][j]["field"] = "land"
		elif animal == Animal.SHARK:
			grid[i][j]["field"] = "sea"
		animalCount[animal] -= 1
		if animalCount[animal] == 0:
			possibleAnimals.remove(animal)

	#on génère un emplacement de départ
	#Attention : l'emplacement peut être n'importe ou, possibilité de départs aléatoires
	startPoint = random.choice(safeCells)

	exportMap(gridName, grid, startPoint, M, N)

# génère un triplet d'animaux aléatoire dans la carte à placer
# M,N : taille de la grille
# minAnimalRate, maxAnimalRate : taux d'occupation min et max des animaux dans la map
def generateAnimalTriplet(M: int, N: int, minAnimalRate: float, maxAnimalRate: float):
	if minAnimalRate > 1 or maxAnimalRate > 1 or minAnimalRate < 0 or maxAnimalRate < 0:
		print("Error in generateAnimalTriplet : please give animal rate between 0 and 1")
		exit()
	mapSize = M*N
	minAnimalCount = int(minAnimalRate * mapSize)
	maxAnimalCount = int(maxAnimalRate * mapSize)
	totalAnimalCount = random.randint(minAnimalCount, maxAnimalCount)
	# on génère un triplet dont la somme est égale à totalAnimalCount
	triplet = [random.randint(0, totalAnimalCount), random.randint(0, totalAnimalCount), random.randint(0, totalAnimalCount)]
	if triplet != [0, 0, 0]:
		triplet = triplet/np.sum(triplet, axis=0) * totalAnimalCount
	triplet = [int(a) for a in triplet]
	# on regarde si il y a des corrections à faire après avoir arrondi le nombre
	remaining = totalAnimalCount - np.sum(triplet, axis=0)
	while remaining != 0:
		i = np.random.randint(2)
		if remaining > 0:
			triplet[i] += 1
			remaining -= 1
		else:
			triplet[i] -= 1
			remaining += 1
	return triplet

def main():
	if len(sys.argv) == 1:
		print("Usage : python3 " + sys.argv[0] + " [number_of_maps]")
		print("Optional argument : python3 " + sys.argv[0] + " [number_of_maps] --verbose")
		exit()
	nbMaps = int(sys.argv[1])
	minGridSize = 3
	maxGridSize = 65
	meanAnimalCoverage = 0
	# on génère 3 niveaux de difficulté en fonction de la taille des maps
	lvl = [int(nbMaps/3),2*int(nbMaps/3),nbMaps-1]
	for i in range(nbMaps):
		#1/3rd of the map will be of one type of level from easy to hard
		if i < lvl[0]:
			M = random.randint(minGridSize, 20)
			N = random.randint(minGridSize, 20)
			#animal will occupate between 10% of size of the map and 30%
			gridName = "grid" + str(i) + "-easy"
		elif i < lvl[1]:
			M = random.randint(minGridSize, 35)
			N = random.randint(minGridSize, 35)
			#animal will occupate between 10% of size of the map and 30%
			gridName = "grid" + str(i) + "-medium"
		else:
			M = random.randint(minGridSize, maxGridSize)
			N = random.randint(minGridSize, maxGridSize)
			#animal will occupate between 10% of size of the map and 30%
			gridName = "grid" + str(i) + "-hard"

		animalCount = generateAnimalTriplet(M, N, 0.1, 0.3)
		landProb = random.random()
		currentAnimalCoverage = sum(animalCount) / (M*N)
		meanAnimalCoverage += currentAnimalCoverage
		if len(sys.argv) > 2 and sys.argv[2] == "--verbose":
			print("generating gen-" + gridName + ".croco ...")
			print("  - Size : " + str(M) + "x" + str(N))
			print("  - Animals : ", end="")
			print(sum(animalCount), end=" ")
			print(animalCount)
			print("  - Animal coverage : {:.2f}".format(currentAnimalCoverage * 100), end="%\n")
			print("  - Land influence factor : {:.2f}".format(landProb))
		generateMap(gridName, M, N, animalCount, landProb)
	meanAnimalCoverage = meanAnimalCoverage / nbMaps
	print("Done.")
	print("Mean animal coverage : {:.2f}".format(meanAnimalCoverage * 100), end="%\n")

if __name__ == "__main__":
    main()
