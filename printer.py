"""
@file printer.py
Définit les fonctions d'affichage de la grille, si l'option -p est activée
Class GridPrinter qui contient des méthodes statiques
"""

from enumAnimal import Animal
from debug_utilities import Debug

class GridPrinter:
	@staticmethod
	def printGrid(grid):
		if Debug.instance().debugP:
			print("======Print Grid=======")
			M = len(grid)
			for i in range(-1, M):
				N = len(grid[i])
				for j in range(-1, N):
					# printing row and column numbers
					if i == -1:
						if j == -1:
							print("  ", end="")
						if j != N-1:
							print(str(j+1), end=" ")
					elif j == -1:
						print(str(i), end=" ")
					# printing map
					else:
						if (grid[i][j]["type"] == Animal.IGNORE):
							print("|", end=" ")
						elif (grid[i][j]["type"] == Animal.UNKNOWN):
							print("?", end=" ")
						elif (grid[i][j]["type"] == Animal.NO_ANIMAL):
							field = grid[i][j]["field"]
							if field == "sea":
								print("~", end=" ")
							elif field == "land":
								print("-", end=" ")
							else:
								print("#", end=" ")
						elif (grid[i][j]["type"] == Animal.TIGER):
							print("T", end=" ")
						elif (grid[i][j]["type"] == Animal.SHARK):
							print("S", end=" ")
						elif (grid[i][j]["type"] == Animal.CROCODILE):
							field = grid[i][j]["field"]
							if field == "sea":
								print("W", end=" ")
							elif field == "land":
								print("C", end=" ")
							else:
								print("c", end=" ")
				print("\n")

	@staticmethod
	def printCell(grid, i, j):
		if (grid[i][j]["type"] == Animal.IGNORE):
			print("|", end=" ")
		elif (grid[i][j]["type"] == Animal.UNKNOWN):
			print("?", end=" ")
		elif (grid[i][j]["type"] == Animal.NO_ANIMAL):
			if "field" in grid[i][j]:
				field = grid[i][j]["field"]
				if field == "sea":
					print("~", end=" ")
				elif field == "land":
					print("-", end=" ")
			else:
				print("#", end=" ")
		elif (grid[i][j]["type"] == Animal.TIGER):
			print("T", end=" ")
		elif (grid[i][j]["type"] == Animal.SHARK):
			print("S", end=" ")
		elif (grid[i][j]["type"] == Animal.CROCODILE):
			field = grid[i][j]["field"]
			if field == "sea":
				print("W", end=" ")
			elif field == "land":
				print("C", end=" ")
			else:
				print("c", end=" ")

	@staticmethod
	def printGridAndGridGuess(grid, gridGuess):
		if Debug.instance().debugP:
			print("==== Print Grid & Grid Guess =====")
			M = len(grid)
			for i in range(-1, M):
				N = len(grid[i])
				#print grid
				for j in range(-1, N):
					if i == -1:
						if j == -1:
							print("  ", end="")
						if j != N-1:
							print(str(j+1), end=" ")
					elif j == -1:
						print(str(i), end=" ")
					else:
						GridPrinter.printCell(grid, i, j)
				#print grid guess
				print("  ", end="")
				for j in range(N):
					if i == -1:
						print(str(j), end=" ")
					else:
						GridPrinter.printCell(gridGuess, i, j)
				print("\n")
