"""
@file project_tying.py
Définit les alias de types utilisés dans le projet
"""

from typing import List, Tuple, Dict

# aliases de type
Variable = int
Clause = List[Variable]
GrilleDict = List[List[Dict]]

# types de retour du serveur
Status = str # "OK"|"KO"|"Err"|"GG"
Msg = str
Info = Dict
# Info = {
#     "pos": (int, int), # (i, j) i < M, j < N
#     "field": str, # "sea"|"land"
#     "prox_count": [int, int, int] # (tiger_count, shark_count, croco_count), optional
# }
Infos = List[Info]
