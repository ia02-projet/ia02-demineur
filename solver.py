"""
@file solver.py
Fichier main du projet. Parse les arguments en ligne de commande,
se connecte au serveur, setup le niveau de debug et lance le jeu
"""

# python std lib import
from typing import List, Tuple, Dict
import subprocess
import time
import signal
import traceback
import sys
import getopt


# project files import
from crocomine_client import CrocomineClient
from solver_utilities import SolverUtil
from map_solver import MapSolver
from printer import GridPrinter
from enumAnimal import Animal
from debug_utilities import Debug

# Global
externalServer = False
serverAdress = "http://localhost:8000"
statExport = False

# ==================================================================================
# ==================================== Main Utils ==================================
# ==================================================================================

def argumentParsing():
	debug = Debug.instance()
	#On récupère les arguments sous forme d'options
	options, remain = getopt.getopt(sys.argv[1:], "dpms:ch", ["debug", "print", "server-map", "server", "csv", "help"])
	#On parse les arguments en fonction des options
	global externalServer
	global serverAdress
	for opt, arg in options:
		if opt in ("-d", "--debug"):
			debug.debug = True
			debug.debugP = True
		elif opt in ("-p", "--print"):
			debug.debugP = True
		elif opt in ("-s", "--server"):
			serverAdress = arg
			print("Launchine on external server " + serverAdress + "...")
			externalServer = True
		elif opt in ("-m", "--server-map"):
			debug.server_map = True
		elif opt in ("-c", "--csv"):
			global statExport
			statExport = True
		elif opt in ("-h", "--help"):
			printHelp()
			quit()

def printHelp():
	print("usage: python3 solver.py [options] ... ")
	print("Options and arguments:")
	print("-d, --debug")
	print("\tLaunch in debug mode, printing and pausing at each action")
	print("-p, --print")
	print("\tPrint debug informations, without pausing")
	print("-m, --server-map")
	print("\tPrint beautify server map evolution.")
	print("-s [server_adress], --server [server_adress]")
	print("\tUse an external server")
	print("-c, --csv")
	print("\tStat export in csv : file stats.csv")
	print("-h, --help")
	print("\tDisplay this help")

# ==================================================================================
# ======================================= Main =====================================
# ==================================================================================

# démarre les grille en appelant new_grid sur le serveur, jusqu'à ce que toutes les
# grilles aient été jouées
# exécute discover sur la case de départ
def play(client: CrocomineClient) -> Tuple[Tuple[int,int],Tuple[int,int]]:
	debug = Debug.instance()
	mapCount = 0
	print("=============== Start Playing ================== \n")

	maps = True
	totalServerCalls = 0
	mapWin = 0
	mapLost = 0
	# Adding the headers for the stat file
	if statExport:
		with open("stats.csv", "w", newline="") as stat:
			stat.write("name;win;num;M;N;time;calls\n")
			stat.close()

	# Iteration on the maps given by the server
	while(maps):
		status, msg, gridInfos = client.new_grid()

		debug.debugPrint(status +" : "+ msg)
		debug.debugPprint(gridInfos)

		serverCalls = 0
		# Playing the map
		if (status == "OK"):
			debug.infoPrint("Playing : " + msg)

			# Stat export the map name
			if statExport:
				with open("stats.csv", "a", newline="") as stat:
					stat.write(msg+";")

			mapCount += 1

			# Map size
			M = gridInfos["m"]
			N = gridInfos["n"]
			print("size "+str(M)+";"+str(N))

			# Local grid instanciation
			grid = [ [{"type": Animal.IGNORE} for i in range(N)] for j in range(M)]

			start_time = time.time()
			# on découvre la case de départ
			status, msg, infos = client.discover(gridInfos["start"][0], gridInfos["start"][1])
			serverCalls = 1
			if status == "OK":
				debug.debugPprint(infos)

				# Updating the grid with new informations
				SolverUtil.updateGrid(grid, infos)
				GridPrinter.printGrid(grid)

				# resolving the map
				partialServerCall, win = MapSolver.runMap(grid, client, gridInfos)
				serverCalls += partialServerCall

				# Counting the Win and lost map
				if win:
					# Stat export winning status
					if statExport:
						with open("stats.csv", "a", newline="") as stat:
							stat.write("GG"+";")
					mapWin += 1
				else:
					# Stat export winning status
					if statExport:
						with open("stats.csv", "a", newline="") as stat:
							stat.write("KO"+";")
					mapLost += 1
			elif status == "GG":
				debug.infoPrint("Map finished : "+status)
				mapWin += 1
			else:
				# Should not append on a safe cell
				debug.infoPrint(status + ": Server Error : " + msg)
				debug.infoPrint("Problem discovering the safe starting point")
				debug.infoPrint("Loading next map \n")

			exec_time = time.time() - start_time

			# Printing map informations
			debug.infoPrint("  Map n° " +str(mapCount)+", size("+str(M)+";"+str(N)+") resolved in : "+str(exec_time))
			debug.infoPrint("  Actions envoyées au serveur : " + str(serverCalls) + "\n")

			# Export map informations stats
			if statExport:
				with open("stats.csv", "a", newline="") as stat:
					stat.write(str(mapCount)+";"+str(M)+";"+str(N)+";"+str(exec_time)+";"+str(serverCalls)+"\n")
		else:
			# If Err status : no more map to play
			maps = False
		totalServerCalls += serverCalls

	return ((mapCount,totalServerCalls), (mapWin, mapLost))

# Se connecte au serveur et renvoie le client associé
def connecToServ() -> CrocomineClient:
	try:
		server = serverAdress
		group = "Groupe 3"
		members = "Victor et Bastien"
		client = CrocomineClient(server, group, members, "bbvk*")
		return client
	except:
		raise Exception("Impossible to connect to the server " + server)

# lance le serveur comme un processus à part
def launchServer():
	if Debug.instance().server_map:
		return subprocess.Popen(["./server/linux64/crocomine-lite",
		                                  ":8000",
		                                  "./grids/"], stderr=subprocess.DEVNULL)
	else:

		return subprocess.Popen(["./server/linux64/crocomine-lite",
		                                  ":8000",
		                                  "./grids/"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def main():
	argumentParsing()
	debug = Debug.instance()
	crocoMineProc = None
	try:
		if not externalServer:
			crocoMineProc = launchServer()
			time.sleep(1)

		client = connecToServ()

		start_time = time.time()

		# Playing the map pool and receiving informations on the play
		(num_map, serverCalls), (mapWin, mapLost) = play(client)

		exec_time = time.time() - start_time

		# Print stats of the played map pool
		debug.infoPrint("Global time : " + str(exec_time))
		debug.infoPrint("Number of map played : " + str(num_map))
		if (num_map > 0):
			debug.infoPrint("Average time per map : " + str(exec_time/num_map))
		debug.infoPrint("\twin : " + str(mapWin) + "\tlost : " + str(mapLost))
		debug.infoPrint("Number of server call : " + str(serverCalls))

		if not externalServer:
			crocoMineProc.kill()
	except BaseException:
		traceback.print_exc()
		if not externalServer:
			if crocoMineProc is not None:
				crocoMineProc.kill()

if __name__ == "__main__":
    main()
