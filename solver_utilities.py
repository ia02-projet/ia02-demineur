"""
@file solver_utilities
Contient les classes et fonctions nécessaires à la modélisation
de la grille en format dimacs
"""

from typing import List, Tuple, Dict
import copy
from math import factorial

from enumAnimal import Animal, strAnimalToEnum, enumAnimalToStr
from dimacs import Dimacs
from clausal_constraints import exactly_n

from project_typing import Variable, GrilleDict, Infos
from debug_utilities import Debug

#Global variables
M = 5
N = 5
MAX_COMBINATIONS = 1000000
MAX_REMAINING_CELL_ANIMAL_COUNT = 200
#nombre de variables booléennes par case
#de 0 à 4 : 0 pour tigre, 1 pour requin, 2 pour croco, 3 pour zone safe
variableCountPerCell = 4

class SolverUtil:

	@staticmethod
	def updateGrid(grid: GrilleDict, infoServ: Infos):
		global M
		global N
		M=len(grid)
		N=len(grid[0])
		for info in infoServ:
			i = info["pos"][0]
			j = info["pos"][1]
			grid[i][j]["field"] = info["field"]
			# après un guess, info renvoie l'animal qu'on a deviné
			if "animal" in info:
				grid[i][j]["type"] = strAnimalToEnum(info["animal"])
			# si prox_count est dans l'info de la case, c'est qu'il n'y a pas d'animal dessus
			if ("prox_count" in info):
				grid[i][j]["type"] = Animal.NO_ANIMAL # représente l'animal sur la case (i,j)
				grid[i][j]["prox_count"] = info["prox_count"]
			else:
				# dès qu'on a reçu une info du serveur sur la case, on la passe de IGNORE à UNKNOWN
				# IGNORE permet de filtrer les cases sur lesquels il ne faut pas chercher car on a aucune info
				if grid[i][j]["type"] == Animal.IGNORE:
					grid[i][j]["type"] = Animal.UNKNOWN

	#renvoie les coordonnées des cases qui sont à proximité de la case en paramètre
	#filtre les cases par le critère "déjà découverte"
	@staticmethod
	def getProx(grid: GrilleDict, i: int, j: int) -> Tuple[List[Tuple[int, int]], List[Tuple[int, int]]]:
		global M
		global N
		M=len(grid)
		N=len(grid[0])
		undiscoveredProx = []
		discoveredProx = []
		for m in range(i-1,i+2):
			for n in range(j-1, j+2):
				if (m,n) != (i,j):
					if m >= 0 and m < M and n >= 0 and n < N:
						if grid[m][n]["type"] ==  Animal.UNKNOWN: #case à proximité non découverte
							undiscoveredProx.append((m,n))
						else:
							discoveredProx.append((m,n))
		return (undiscoveredProx, discoveredProx)

	#modélise la grille en format dimacs avec des variables booléennes
	@staticmethod
	def gridToDimacs(grid: GrilleDict):
		global M
		global N
		M=len(grid)
		N=len(grid[0])
		cnf = Dimacs(M*N*variableCountPerCell)
		for i in range(M):
			for j in range(N):
					cell = grid[i][j]
					# on encode l'unicité du type d'une case
					cnf.addClauses(exactly_n(1, cellVariables(i,j)))
					#si la case a été découverte et qu'on connait son type
					if cell["type"] != Animal.UNKNOWN and cell["type"] != Animal.IGNORE:
					    cnf.addClause([cell_to_variable(i,j,cell["type"])])
					#si la case possède des voisins dangereux, on génère les combinaisons de voisins possibles
					if "prox_count" in cell:
						#on récupère les voisins de la cellule
						undiscoveredProxCells, discoveredProxCells = SolverUtil.getProx(grid, i, j)
						if undiscoveredProxCells != []:
							#on récupère le nombre d'animaux non découverts à proximité de la case
							undiscoveredProxCount = substractDiscoveredAnimals(cell, grid, undiscoveredProxCells, discoveredProxCells)

							#récupération des listes de variables des cellules voisines par type
							#noList : liste des impossibilités de trouver un animal sur un type de case
							listTiger, noListTiger = animalVariables(undiscoveredProxCells, Animal.TIGER, grid)
							listShark, noListShark = animalVariables(undiscoveredProxCells, Animal.SHARK, grid)
							listCroc, noListCroc = animalVariables(undiscoveredProxCells, Animal.CROCODILE, grid)

							#on encode les règles correspondant au voisinage de la case
							cnf.addClauses(exactly_n(undiscoveredProxCount[Animal.TIGER], listTiger))
							cnf.addClauses(exactly_n(undiscoveredProxCount[Animal.SHARK], listShark))
							cnf.addClauses(exactly_n(undiscoveredProxCount[Animal.CROCODILE], listCroc))
							cnf.addClauses(exactly_n(0,noListCroc))
							cnf.addClauses(exactly_n(0,noListShark))
							cnf.addClauses(exactly_n(0,noListTiger))
		return cnf

	# ajoute la contrainte globale du nombre d'animaux qu'il reste à découvrir
	# la contrainte est ajoutée si elle respecte certains critères (voir README) pour
	# ne pas trop ralentir le programme 
	@staticmethod
	def useAnimalCountDimacs(animalCount : List[int], cnf : Dimacs, cells : List[Tuple[int,int]], numUnkownPerihpCell : int, numIgnoreCell : int, grid : GrilleDict) -> bool:
		success_count = 0
		#if len(cells) <= MAX_REMAINING_CELL_ANIMAL_COUNT:

		if numIgnoreCell < numUnkownPerihpCell:
			if animalCount[Animal.TIGER] != None:
				listTiger, noListTiger = animalVariables(cells, Animal.TIGER, grid)
				try:
					combinations = factorial(len(listTiger))//(factorial(animalCount[Animal.TIGER])*factorial(len(listTiger)-animalCount[Animal.TIGER]))
					if combinations < MAX_COMBINATIONS:
						success_count += 1
						cnf.addClauses(exactly_n(animalCount[Animal.TIGER],listTiger))
						cnf.addClauses(exactly_n(0,noListTiger))
				except:
					Debug.instance().debugPrint("Combinations Too Large")

			if animalCount[Animal.SHARK] != None:
				listShark, noListShark = animalVariables(cells, Animal.SHARK, grid)

				try:
					combinations = factorial(len(listShark))//(factorial(animalCount[Animal.SHARK])*factorial(len(listShark)-animalCount[Animal.SHARK]))
					if combinations < MAX_COMBINATIONS:
						success_count += 1
						cnf.addClauses(exactly_n(animalCount[Animal.SHARK],listShark))
						cnf.addClauses(exactly_n(0,noListShark))
				except:
					Debug.instance().debugPrint("Combinations Too Large")

			if animalCount[Animal.CROCODILE] != None:
				listCroco, noListCroco = animalVariables(cells, Animal.CROCODILE, grid)

				try:
					combinations = factorial(len(listCroco))//(factorial(animalCount[Animal.CROCODILE])*factorial(len(listCroco)-animalCount[Animal.CROCODILE]))
					if combinations < MAX_COMBINATIONS:
						success_count += 1
						cnf.addClauses(exactly_n(animalCount[Animal.CROCODILE],listCroco))
						cnf.addClauses(exactly_n(0,noListCroco))
				except:
					Debug.instance().debugPrint("Combinations Too Large")

		if success_count > 0:
			return False
		else:
			return True

# transforme le triplet de coordonnées (i,j,typeCell) en une variable booléenne
def cell_to_variable(i: int, j: int, val: int) -> int:
    # val : 0 pour tigre, 1 pour requin, 2 pour croco, 3 pour zone safe
    return i*N*variableCountPerCell + j*variableCountPerCell + val + 1

# transforme une variable booléenne en un triplet de coordonnées (i,j,typeCell)
def variable_to_cell(lit: int) -> Tuple[int, int, int]:
    lit -= 1
    val = lit % (variableCountPerCell)
    lit -= val
    j = (lit//(variableCountPerCell))%N
    lit -= j*variableCountPerCell
    i = lit // (N*variableCountPerCell)
    return (i,j,val)

#retourne l'ensemble des variables booléennes représentant une case de la grille
def cellVariables(i: int, j: int) -> List[Variable]:
    cellVariables = []
    for v in range(variableCountPerCell):
        cellVariables.append(cell_to_variable(i,j,v))
    return cellVariables

#retourne l'ensemble des variables correspondant à un certain animal pour chaque cellule donnée en paramètre
#retourne une première liste des possibilités de trouver un tel animal sur un type de case
#retourne une seconde liste des impossibilités de trouver un tel animal sur un type de case
def animalVariables(cells: List[Tuple[int, int]], animal: int, grid: GrilleDict) -> Tuple[List[Variable], List[Variable]]:
	animalVariables = []
	noAnimalVariables = []
	for c in cells:
		i,j = c
		cell = grid[i][j]
		if (animal == Animal.CROCODILE) or (animal == Animal.IGNORE) or ("field" not in cell):
			animalVariables.append(cell_to_variable(c[0], c[1], animal))
		elif animal == Animal.SHARK:
			if cell["field"] == "sea" :
				animalVariables.append(cell_to_variable(c[0], c[1], animal))
			else:
				noAnimalVariables.append(cell_to_variable(i,j,animal))
		elif animal == Animal.TIGER:
			if cell["field"] == "land" :
				animalVariables.append(cell_to_variable(i, j, animal))
			else:
				noAnimalVariables.append(cell_to_variable(i,j,animal))
	return (animalVariables,noAnimalVariables)

# Renvoie le nombre de tigre, de requins et de crocodiles non découverts à proximité de la case "cell"
# Prend en paramètre la liste des cases voisines de la case, divisée en 2 :
# case déjà découvertes, et cases non découverte.
def substractDiscoveredAnimals(cell: Dict, grid: GrilleDict,
                                      undiscoveredProxCells: List[Tuple[int, int]],
                                      discoveredProxCells: List[Tuple[int, int]]) -> Tuple[int, int, int]:
	undiscoveredProxCount = copy.deepcopy(cell["prox_count"])
	for k in range(len(discoveredProxCells)):
		(x,y) = (discoveredProxCells[k][0], discoveredProxCells[k][1])
		typeCurrentCell = grid[x][y]["type"]
		if typeCurrentCell != Animal.NO_ANIMAL:
			if undiscoveredProxCount[typeCurrentCell] > 0:
				undiscoveredProxCount[typeCurrentCell] -= 1
	return undiscoveredProxCount
