"""
@file: enumAnimal.py
Implémentation de l'énumération Animal, qui définit les différents types de cases
dans le jeu
"""

from enum import IntEnum

class Animal(IntEnum):
    TIGER = 0
    SHARK = 1
    CROCODILE = 2
    NO_ANIMAL = 3
    IGNORE = 4 #cell marked IGNORE are cells with no informations
    UNKNOWN = 5 #cell marked UNKNOWN are cell that we have to guess the type

def strAnimalToEnum(animal: str) -> int:
	if animal == 'T':
		return Animal.TIGER
	if animal == 'C':
		return Animal.CROCODILE
	if animal == 'S':
		return Animal.SHARK
	return Animal.NO_ANIMAL

def enumAnimalToStr(animal: int) -> str:
	if animal == Animal.TIGER:
		return "T"
	if animal == Animal.SHARK:
		return "S"
	if animal == Animal.CROCODILE:
		return "C"
	return "Safe"
